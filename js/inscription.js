
$(function(){
    // ANNULE LE formulaire d'ajout de réservation
    $(".close_mod_ins").on("click",function(){
        $("#add_ins_mod").modal("hide");
        $("#form_add_ins")[0].reset();
    });

    //Ouvre la modale d'inscription
    $(".add_ins").on("click",function(){
        //   $("#add_ins_mod").modal("show");
        $("#add_ins_mod #id_res").val($(this).attr('id_res'));
    });


    $("#inscription_form").validate(
        {
            //debug:true,
            rules:{
                nom_ins:{
                    required:true,
                    minlength:2
                },

                prenom_ins:{
                    required:true,
                    minlength:2
                },

                email_ins:{
                    required:true,
                    email:true

                },
                tel_ins:{
                    required:true,
                    /*number: true,*/
                    minlength:10

                },
                categorie_ins:{
                    required:true
                },
                nb_elv_ins:{
                    min: 0
                    //max:14-num
                },
                nomclub_ins:{
                    minlength:2,

                }

            },


            messages:{
                nom_ins:{
                    required: "Veuillez saisir votre nom",
                    minlength: "Votre nom doit être composé de 2 caractères au minimum"
                },

                prenom_ins:{
                    required: "Veuillez saisir votre prénom",
                    minlength: "Votre prénom doit être composé de 2 caractères au minimum"
                },

                email_ins:{
                    required: "Veuillez saisir votre mail",
                    email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                },
                tel_ins:{
                    required:"Veuillez saisir votre numéro de téléphone",
                    number: "Votre téléphone doit avoir le format suivant : XXX.XXX.XXXX",
                    length:"Votre téléphone doit avoir le format suivant : XXX.XXX.XXXX"
                },
                categorie_ins:{
                    //required:function() {
                    //   $("#label_categorie").css("color","red");
                    //}
                    required:"Veuillez choisir la catégorie"

                },
                nb_elv_ins:{
                    required:"Veuillez saisir le nombre d'élèves",
                    min: "impossible d'avoir des élèves négatifs",
                    max: ""

                },
                nom_club_ins:{
                    required:"Veuillez saisir le nom du club ou d'école",
                    minlength: "Le nom doit être composé de 2 caractères au minimum",
                }

            },
            submitHandler: function() {
                if($('#type_ins').is(':checked')) {
                    type_ins = 1;
                }else{
                    type_ins = 0;
                }
                if($('#club_ins').is(':checked')) {
                    club_ins = 1;
                }else{
                    club_ins = 0;
                }



                $.post(
                    "./json/add_ins.json.php",
                    {
                        nom_ins: $("#nom_ins").val(),
                        prenom_ins: $("#prenom_ins").val(),
                        tel_ins: $("#tel_ins").val(),
                        email_ins: $("#email_ins").val(),

                        categorie_ins: $("[name='categorie_ins']:checked").val(),
                        type_ins: type_ins,
                        nb_elv_ins: $("#nb_elv_ins").val(),
                        club_ins : club_ins,
                        nom_club_ins: $("#nom_club_ins").val(),
                        id_res:  $("#id_res").val()
                    },
                    function(){
                        $("#add_ins_mod").modal("hide");
                        //$("#form_add_ins")[0].reset();
                        location.reload();
                    }
                )
            }
        }
    )



    //faire apparaitre les champs no. d'élèves et nom du club/école


    $("#type_ins").change(function() {
        if (num = 0) {
            $('#type_ins').css("display", "none");
        }else{
            if (this.checked) {
                $('#eleve').css("display", "block");
            }
            else {
                $('#eleve').css("display", "none");
            }
        }
    });

    $("#club_ins").change(function() {
        if(this.checked) {
            $('#club').css("display","block");
        }
        else{
            $('#club').css("display","none");
        }
    });


    //champs no. d'élèves et nom du club/àcole required si moniteur ou membre d'un club coché respectivement


    $('#type_ins').change(function() {

        if ($('#eleve_ins').attr('required')) {
            $('#eleve_ins').removeAttr('required');
        }

        else {
            $('#eleve_ins').attr('required','required');
        }

    });

    $('#club_ins').change(function() {

        if ($('#nomclub_ins').attr('required')) {
            $('#nomclub_ins').removeAttr('required');
        }
        else {
            $('#nomclub_ins').attr('required','true');
        }

    });


    //ne pas envoyer les données no. d'élèves ou nom du club si les cases respectives ne sont pas cochées


    $( "#inscription_form" ).submit(function() {
        if ($('#type_ins').is(!':checked')) {
            $(this.nb_elv_ins).remove();
        }
    });

    $( "#inscription_form" ).submit(function() {
        if ($('#club_ins').is(!':checked')) {
            $(this.nomclub_ins).remove();
            return true;
        }
    });
});