$(function(){

    $.validator.addMethod("PWCHECK", function(value, element) {

        return /^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value);
    });

    $("#login_form").validate(
        {
            rules:{
                email_per: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    PWCHECK: true
                }
            },
            messages: {
                email_per: {
                    required: "Veuillez saisir votre email",
                    email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                },
                password: {
                    required: "Veuillez saisir votre mot de passe",
                    PWCHECK: "Le mot de passe doit comporter au minimum 8 caractères, dont une minuscule, une majuscule, un chiffre et un caractère spécial"
                }
            },
            submitHandler: function(form) {

                $.post(
                    "./json/login.json.php?_="+Date.now(),
                    {
                        email_per: $("#email_per").val(),
                        password: $("#password").val()
                    },
                    function result(data, status){

                        message(data.message.texte, data.message.type);

                        if(data.message.type == "success") {

                            location.replace("./reservations/index.php");
                        }
                    }
                );
            }
        }
    );
});
