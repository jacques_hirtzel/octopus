
$(function(){
    $(".confirmation").on("click",function (){
        $.post(
            "./json/edit_ins.json.php",
            {
                nb_elv_ins: $("#nb_elv").val(),
                id_ins: $(this).attr("id_ins")

            },
            function () {
                location.assign("./detailsAdmin.php?id_res="+$(".confirmation").attr("id_res"));
            }
        )
    });

    $(".suppression").on("click",function (){
        id_ins = $(this).attr("id_ins");
        bootbox.confirm({
            title: "Supprimer une inscription",
            message: "Êtes-vous sûr de vouloir <b>supprimer cette inscription</b><br>Merci d'avertir les participants avant de supprimer",
            buttons: {
                cancel: {
                    label: 'Annuler'
                },
                confirm: {
                    label:  'Supprimer'
                }
            },
            callback: function (result) {
                if(result == true) {
                    del_res(id_ins);
                }
            }
        });
    });

    function del_res(id_ins){
        $.post(
            "./json/del_ins.json.php",
            {
                id_ins: id_ins
            },
            function(data){
                location.reload();
            }
        )
    }
});