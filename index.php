<?php
require_once substr(__dir__, 0, strpos(__dir__, "octopus")+strlen("octopus")) . "/config/config.inc.php";
/*$aut = "USR_INT";
require(WAY . "/includes/secure.inc.php");
header("Location: ". URL . "/reservations/index.php");*/
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>


<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h3>Réservations à venir</h3>
            <p><a id="conditions_reservation" target="_blank" href="<?= URL_CONDITIONS ?>">Conditions de réservation</a></p>
        </div>

        <div class="panel-body">

            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>Date</th>
                    <th>Nom Réservation</th>
                    <th>Responsable</th>
                    <th>Nb. Participants</th>
                    <th></th>

                </tr>
            <?php
            $res = new Reservation();
            $tab_res = $res->get_res_a_venir();
            foreach ($tab_res as $reservation) {
                $res = new Reservation($reservation['id_res']);
                $per_res= new Personne($reservation['id_per']);
                $date_res = date("d.m.Y", strtotime($reservation['date_res']));
                ?>
                <tr>
                    <td <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?=$date_res?></td>
                    <td <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?= $res->get_texte()?></td>
                    <td <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?= $per_res->get_nom()." ".$per_res->get_prenom()?></td>
                    <td <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?= $res->get_nb_participants() ?></td>
                    <td>
                        <?php
                        if($reservation['close_res'] == 1) {
                            ?>
                            <a href="details.php?id_res=<?= $res->get_id() ?>">
                                <button class="btn btn-primary detail_ins" id_res="<?= $res->get_id() ?>">Détails
                                </button>
                            </a>
                            <?php
                            /*if($res->get_nb_participants() >= 15) {
                                echo "Complet";
                            }else{*/
                            ?>
                            <button class="btn btn-primary add_ins" data-toggle="modal" data-target="#add_ins_mod"
                                    id_res="<?= $reservation['id_res'] ?>">S'inscrire
                            </button>
                            <?php
                        }
                        else{
                            echo "Fermé";
                        }
                            //}
                            ?>

                    </td>
                </tr>

            <?php
            }
            ?>
            </table>
            <a href="login.php" ><button class="btn btn-primary">Se connecter</button></a>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>
<?php
include (WAY."mod/inscription.mod.php");
?>
<script src="./js/inscription.js"></script>
