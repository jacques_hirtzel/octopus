<?php

define("DISPLAY_ERROR", 0);
error_reporting(E_ALL);
ini_set("display_errors", DISPLAY_ERROR);


//emplacement du projet octopus sur le disque dur. Par exemple web/octopus/
define("WAY", "");

//URL d'accès au site internet
define("URL", "http://");

//nom de la base de données
define("BASE_NAME", "octopus");

//hôte de la base de données
define("SQL_HOST", "localhost");

//Nom d'utilisateur de l'utilisateur de la base de données
define("SQL_USER", "");

//Mot de passe de l'utilisateur de la base de données
define("SQL_PASSWORD", "");

//Lien vers les conditions de réservation visible sur la page publique en dessous du titre
define("URL_CONDITIONS", "http://www.octopus-diving.ch/");

//Adresse Email Utilisée pour l'envoi du mail de récupération de mot de passe
define("MAIL_WEBMASTER", "test@exemple.ch");
?>