-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 18 sep. 2019 à 10:22
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `octopus`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_autorisations`
--

CREATE TABLE `t_autorisations` (
  `id_aut` int(10) UNSIGNED NOT NULL,
  `nom_aut` varchar(100) NOT NULL,
  `code_aut` varchar(10) NOT NULL,
  `desc_aut` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_autorisations`
--

INSERT INTO `t_autorisations` (`id_aut`, `nom_aut`, `code_aut`, `desc_aut`) VALUES
(1, 'Utilisateurs', 'ADM_USR', 'Gestion des utilisateurs'),
(4, 'Administrations des autorisations', 'ADM_AUT', 'Administrations des autorisations'),
(8, 'Gestion des réservations', 'ADM_RES', 'Gestion des réservations');

-- --------------------------------------------------------

--
-- Structure de la table `t_aut_fnc`
--

CREATE TABLE `t_aut_fnc` (
  `id_aut` int(10) UNSIGNED NOT NULL,
  `id_fnc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_aut_fnc`
--

INSERT INTO `t_aut_fnc` (`id_aut`, `id_fnc`) VALUES
(1, 1),
(4, 1),
(8, 1),
(8, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_fnc_per`
--

CREATE TABLE `t_fnc_per` (
  `id_fnc` int(10) UNSIGNED NOT NULL,
  `id_per` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_fnc_per`
--

INSERT INTO `t_fnc_per` (`id_fnc`, `id_per`) VALUES
(1, 1),
(3, 1),
(3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `t_fonctions`
--

CREATE TABLE `t_fonctions` (
  `id_fnc` int(10) UNSIGNED NOT NULL,
  `nom_fnc` varchar(100) NOT NULL,
  `abr_fnc` varchar(10) NOT NULL,
  `desc_fnc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_fonctions`
--

INSERT INTO `t_fonctions` (`id_fnc`, `nom_fnc`, `abr_fnc`, `desc_fnc`) VALUES
(1, 'Administrateur', 'Admin', 'Administrateur du site'),
(3, 'Responsable', 'RESP', 'Responsable du samedi');

-- --------------------------------------------------------

--
-- Structure de la table `t_inscriptions`
--

CREATE TABLE `t_inscriptions` (
  `id_ins` int(10) UNSIGNED NOT NULL,
  `nom_ins` tinytext NOT NULL,
  `prenom_ins` tinytext NOT NULL,
  `tel_ins` varchar(20) NOT NULL,
  `email_ins` tinytext NOT NULL,
  `categorie_ins` int(11) NOT NULL,
  `type_ins` int(11) NOT NULL,
  `nb_elv_ins` int(11) NOT NULL DEFAULT '0',
  `club_ins` int(11) NOT NULL,
  `nom_club_ins` tinytext NOT NULL,
  `id_res` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_per` int(10) UNSIGNED NOT NULL,
  `nom_per` varchar(30) NOT NULL,
  `prenom_per` varchar(30) NOT NULL,
  `email_per` tinytext NOT NULL,
  `tel_per` varchar(20) NOT NULL,
  `password_per` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_per`, `nom_per`, `prenom_per`, `email_per`, `tel_per`, `password_per`) VALUES
(1, 'admin', 'admin', 'admin@octopus.ch', '', '$2y$10$kqcmwnumbEw7npJ1KIR43.9OecqJ9hAHFDL4BKKjaRZbeGGKvyWQ6'),
(4, 'Junod', 'Michel', 'mjunod@hispeed.ch', '', '$2y$10$U/oFU9F9toq611jYi75XsOcAh3vfOlVlUeSGJszPOBZPlgJLeLyyy');

-- --------------------------------------------------------

--
-- Structure de la table `t_reservations`
--

CREATE TABLE `t_reservations` (
  `id_res` int(11) UNSIGNED NOT NULL,
  `date_res` date NOT NULL,
  `texte_res` text NOT NULL,
  `id_per` int(11) NOT NULL,
  `close_res` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_reservations`
--

INSERT INTO `t_reservations` (`id_res`, `date_res`, `texte_res`, `id_per`, `close_res`) VALUES
(44, '2022-01-01', 'test', 4, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_autorisations`
--
ALTER TABLE `t_autorisations`
  ADD PRIMARY KEY (`id_aut`);

--
-- Index pour la table `t_aut_fnc`
--
ALTER TABLE `t_aut_fnc`
  ADD PRIMARY KEY (`id_aut`,`id_fnc`),
  ADD UNIQUE KEY `id_aut` (`id_aut`,`id_fnc`),
  ADD KEY `id_fnc` (`id_fnc`);

--
-- Index pour la table `t_fnc_per`
--
ALTER TABLE `t_fnc_per`
  ADD UNIQUE KEY `id_fnc_2` (`id_fnc`,`id_per`),
  ADD KEY `id_fnc` (`id_fnc`),
  ADD KEY `id_per` (`id_per`);

--
-- Index pour la table `t_fonctions`
--
ALTER TABLE `t_fonctions`
  ADD PRIMARY KEY (`id_fnc`);

--
-- Index pour la table `t_inscriptions`
--
ALTER TABLE `t_inscriptions`
  ADD PRIMARY KEY (`id_ins`),
  ADD KEY `id_res` (`id_res`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_per`),
  ADD KEY `id_per` (`id_per`);

--
-- Index pour la table `t_reservations`
--
ALTER TABLE `t_reservations`
  ADD PRIMARY KEY (`id_res`),
  ADD KEY `id_per` (`id_per`),
  ADD KEY `id_res` (`id_res`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_autorisations`
--
ALTER TABLE `t_autorisations`
  MODIFY `id_aut` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `t_fonctions`
--
ALTER TABLE `t_fonctions`
  MODIFY `id_fnc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `t_inscriptions`
--
ALTER TABLE `t_inscriptions`
  MODIFY `id_ins` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_per` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_reservations`
--
ALTER TABLE `t_reservations`
  MODIFY `id_res` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_aut_fnc`
--
ALTER TABLE `t_aut_fnc`
  ADD CONSTRAINT `t_aut_fnc_ibfk_1` FOREIGN KEY (`id_aut`) REFERENCES `t_autorisations` (`id_aut`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_aut_fnc_ibfk_2` FOREIGN KEY (`id_fnc`) REFERENCES `t_fonctions` (`id_fnc`) ON DELETE CASCADE;

--
-- Contraintes pour la table `t_fnc_per`
--
ALTER TABLE `t_fnc_per`
  ADD CONSTRAINT `t_fnc_per_fk_per` FOREIGN KEY (`id_per`) REFERENCES `t_personnes` (`id_per`) ON DELETE CASCADE,
  ADD CONSTRAINT `t_fnc_per_ibfk_1` FOREIGN KEY (`id_fnc`) REFERENCES `t_fonctions` (`id_fnc`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
