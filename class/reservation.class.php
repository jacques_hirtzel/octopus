<?php
Class Reservation EXTENDS Projet{

    // PARAMETERS
    private $id;
    private $texte;
    private $id_per;
    private $date;


    // METHOD: Various
    public function __construct($id = null) {

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }

    }

    public function init() {

        $query = "SELECT * FROM t_reservations WHERE id_res=:id_res";
        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_res'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_date($tab['date_res']);
            $this->set_texte($tab['texte_res']);
            $this->set_id_per($tab['id_per']);

            return true;
        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    public function __toString() {

        $str = "\n<pre>\n";
        foreach($this as $key => $val){
            if($key != "pdo"){
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for($i = $lengh_key; $i < 20;$i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    public function add($tab){

        // Tableau d'arguments
        $args['date_res'] = $tab['date_res'];
        $args['texte_res'] = $tab['texte_res'];
        $args['id_per'] = $tab['id_per'];
        $args['close_res'] = $tab['close_res'];

        // Requête
        $query = "INSERT INTO t_reservations SET 
            date_res = :date_res, 
            texte_res = :texte_res, 
            id_per = :id_per,
            close_res = :close_res";
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();

        } catch (Exception $e) {
            return false;
        }
    }

    public function del($id_res){

        $query = "DELETE FROM t_reservations WHERE 
            id_res = :id_res";
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($id_res);
            return "";

        } catch (Exception $e) {
            return false;
        }
    }

    public function del_ins_by_res($id_res){

        $query = "DELETE FROM t_inscriptions WHERE 
            id_res = :id_res";
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($id_res);
            return "";

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Renoie la totalité des réservation à venir
     * @author J. Hirtzel
     * @date    19.8.2019
     * @return array|bool
     */
    public function get_res_a_venir(){
        // Requête
        $query = "SELECT * FROM t_reservations 
                  WHERE date_res >= current_date
                  ORDER BY date_res ASC";
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll();

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Renoie la totalité des inscriptions d'une réservation
     * @author J. Hirtzel
     * @date    19.8.2019
     * @return array|bool
     */
    public function get_ins(){
        // Requête
        $query = "SELECT * FROM t_inscriptions
                  WHERE id_res=:id_res
                  ORDER BY nom_ins,prenom_ins ASC";
        try {
            $args = array();
            $args['id_res'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $stmt->fetchAll();

        } catch (Exception $e) {
            return false;
        }
    }

    public function get_nb_ins(){
        // Requête
        $query = "SELECT COUNT(id_ins) AS nb_ins FROM t_inscriptions
                  WHERE id_res=:id_res
                  ";
        try {
            $args = array();
            $args['id_res'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['nb_ins'];

        } catch (Exception $e) {
            return false;
        }
    }

    public function get_nb_elv_ins(){
        // Requête
        $query = "SELECT SUM(nb_elv_ins) AS nb_elv 
                  FROM t_inscriptions 
                  WHERE id_res = :id_res";
        try {
            $args = array();
            $args['id_res'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['nb_elv'];
        } catch (Exception $e) {
            return false;
        }
    }

    public function get_nb_participants(){
        // Requête
        $query = "SELECT (SUM(nb_elv_ins) +count(id_ins) ) AS nb_part
                  FROM t_inscriptions 
                  WHERE id_res = :id_res";
        try {
            $args = array();
            $args['id_res'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return $tab['nb_part'];
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function set_id($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function get_texte()
    {
        return $this->texte;
    }

    /**
     * @param mixed $text
     */
    public function set_texte($texte)
    {
        $this->texte = $texte;
    }

    /**
     * @return mixed
     */
    public function get_id_per()
    {
        return $this->id_per;
    }

    /**
     * @param mixed $id_per
     */
    public function set_id_per($id_per)
    {
        $this->id_per = $id_per;
    }

    /**
     * @return mixed
     */
    public function get_date()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function set_date($date)
    {
        $this->date = $date;
    }




}