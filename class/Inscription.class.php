<?php
Class Inscription EXTENDS Projet{
    // PARAMETERS
    private $id;
    private $nom;
    private $prenom;
    private $tel;
    private $email;
    private $categorie;
    private $type;
    private $nb_elv;
    private $club;
    private $nom_club;
    private $id_res;


    // METHOD: Various
    public function __construct($id = null) {

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }

    }

    public function init() {

        $query = "SELECT * FROM t_inscriptions WHERE id_ins=:id_ins";
        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_ins'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_ins']);
            $this->set_prenom($tab['prenom_ins']);
            $this->set_email($tab['email_ins']);
            $this->set_tel($tab['tel_ins']);
            $this->set_categorie($tab['categorie_ins']);
            $this->set_type($tab['type_ins']);
            $this->set_nb_elv($tab['nb_elv_ins']);
            $this->set_club($tab['club_ins']);
            $this->set_nom_club($tab['nom_club_ins']);
            $this->set_id_res($tab['id_res']);
            return true;
        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    public function __toString() {

        $str = "\n<pre>\n";
        foreach($this as $key => $val){
            if($key != "pdo"){
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for($i = $lengh_key; $i < 20;$i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    public function add($tab){
        $reservation = new Reservation($tab['id_res']);

        $to = $tab['email_ins'];
        $subject = "Confirmation cours de plongée";
        $txt = "Ceci est un message automatique veuillez ne pas répondre\n
Bonjour ".$tab['prenom_ins'].",\n
Nous vous Informons que cette adresse email a été utilisée pour une inscription à un cours de plongée le ".date("d.m.Y", strtotime($reservation->get_date())).".\n
Si vous pensez qu'il s'agit d'une erreur veuillez prendre contact avec notre administrateur à l'adresse suivante:\"admin@octopus.ch\".\n
Salutations et bonne soirée,
l'équipe d'Octopus.";
        $headers = "From: confirmation@octopus.ch" . "\r\n" .
            "Content-Type:text;charset=utf-8";

        mail($to,$subject,$txt,$headers);
        // Tableau d'arguments
        $args['nom_ins'] = $tab['nom_ins'];
        $args['prenom_ins'] = $tab['prenom_ins'];
        $args['tel_ins'] = $tab['tel_ins'];
        $args['email_ins'] = $tab['email_ins'];
        $args['categorie_ins'] = $tab['categorie_ins'];
        $args['type_ins'] = $tab['type_ins'];
        $args['nb_elv_ins'] = $tab['nb_elv_ins'];
        $args['club_ins'] = $tab['club_ins'];
        $args['nom_club_ins'] = $tab['nom_club_ins'];
        $args['id_res'] = $tab['id_res'];

        // Requête
        $query = "INSERT INTO t_inscriptions SET 
            nom_ins = :nom_ins, 
            prenom_ins = :prenom_ins, 
            tel_ins = :tel_ins, 
            email_ins = :email_ins, 
            categorie_ins = :categorie_ins, 
            type_ins = :type_ins, 
            nb_elv_ins = :nb_elv_ins, 
            club_ins = :club_ins,
            nom_club_ins = :nom_club_ins, 
            id_res = :id_res";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();

        } catch (Exception $e) {

            return false;
        }
    }

    public function del($id_ins){

        $query = "DELETE FROM t_inscriptions WHERE 
            id_ins = :id_ins";
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($id_ins);
            return "";

        } catch (Exception $e) {
            return false;
        }
    }

    public function edit_nb_elv($tab){

        $args['nb_elv_ins'] = $tab['nb_elv_ins'];
        $args['id_ins'] = $tab['id_ins'];
        $query = "UPDATE t_inscriptions
        SET nb_elv_ins = :nb_elv_ins
        WHERE id_ins = :id_ins";
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return "";

        } catch (Exception $e) {

            return false;
        }
    }

    /**
     * @return mixed
     */
    public function get_id()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function set_id($id)
    {
        $this->id = $id;
    }



    /**
     * @return mixed
     */
    public function get_nom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function set_nom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function get_prenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function set_prenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function get_tel()
    {
        return $this->tel;
    }

    /**
     * @param mixed $tel
     */
    public function set_tel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return mixed
     */
    public function get_email()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function set_email($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function get_categorie()
    {
        return $this->categorie;
    }

    /**
     * @return mixed
     */
    public function get_categorie_txt()
    {
        switch($this->categorie){
            case 1 : return "Plongeur";
            case 2 : return "Nageur";
            case 3 : return "Apnéiste / nageur";

        }
    }


    /**
     * @param mixed $categorie
     */
    public function set_categorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return mixed
     */
    public function get_type()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function get_club_txt()
    {
        switch($this->club){
            case 0 : return "Privé";
            case 1 : return "Club";
        }
    }


    /**
     * @param mixed $type
     */
    public function set_type($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function set_nb_elv($nb_elv){
        $this->nb_elv = $nb_elv;
    }


    /**
     * @return mixed
     */
    public function get_nb_elv(){
        return $this->nb_elv;
    }


    /**
     * @return mixed
     */
    public function  set_club($club){
            $this->club = $club;
    }

    /**
     * @return mixed
     */
    public function  get_club(){
            return $this->club;
    }


    /**
     * @return mixed
     */
    public function get_nom_club()
    {
        return $this->nom_club;
    }

    /**
     * @param mixed $nom_club
     */
    public function set_nom_club($nom_club)
    {
        $this->nom_club = $nom_club;
    }

    /**
     * @return mixed
     */
    public function get_id_res()
    {
        return $this->id_res;
    }

    /**
     * @param mixed $id_res
     */
    public function set_id_res($id_res)
    {
        $this->id_res = $id_res;
    }

    public function get_all(){
        $all = array($this->nom.", ".$this->prenom, $this->tel, $this->email, $this->get_categorie_txt(), $this->get_club_txt(), $this->nom_club, $this->get_nb_elv());
        return $all;
    }


}