<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');

require_once substr(__dir__, 0, strpos(__dir__, "octopus")+strlen("octopus")) . "/config/config.inc.php";

$aut = "ADM_RES";
require(WAY . "/includes/secure.inc.php");

$res = new Reservation();
$res->del($_POST);
$res->del_ins_by_res($_POST);
echo json_encode($_POST);
?>