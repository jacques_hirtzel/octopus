<!-- Fenêtre Modale ajout de réservation -->
<div class="modal fade" id="add_res_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <input type="hidden" name="id_res" id="id_res" value="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="titre_res">Ajout d'une réservation</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form_add_res">
                    <div class="form-group">
                        <label for="date_res" class="col-sm-2 control-label">Date</label>
                        <label class="col-md-3 control-label" id="jour_res"></label>
                        <div class="col-sm-7">
                            <input type="date" class="form-control" id="date_res" name="date_res">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="texte_res" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="texte_res"  id="texte_res">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="texte_res" class="col-sm-2 control-label">Responsable</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="id_per"  id="id_per">
                                <?php
                                $tab_resp = $per->get_all_resp();
                                foreach ($tab_resp AS $per){
                                    echo "<option value=\"".$per['id_per']."\">".$per['nom_per']." ".$per['prenom_per']."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="close_res"  id="close_res"> Fermé
                                </label>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning close_mod_res">Annuler</button>
                <input type="button" class="btn btn-primary" id="btn_add_res" value="réserver">
            </div>
        </div>
    </div>
</div>
