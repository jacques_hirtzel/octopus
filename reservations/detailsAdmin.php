<?php
require_once substr(__dir__, 0, strpos(__dir__, "octopus")+strlen("octopus")) . "/config/config.inc.php";
$aut = "ADM_RES";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
$res = new Reservation($_GET['id_res']);
$per = new Personne($res->get_id_per());
$tab_ins = $res->get_ins();
$date_res = date("d.m.Y", strtotime($res->get_date()));
?>
<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>Détail de la réservation du <?= $date_res?></h1>
            <h2>Responsable : <?= $per->get_nom()." ".$per->get_prenom() ?></h2>
            <p><?= $res->get_texte()?></p>
        </div>

        <div class="panel-body">

            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>Prénom, Nom</th>
                    <th>Nr.Téléphone</th>
                    <th>Email</th>
                    <th>Categorie</th>
                    <th>Club/Privé</th>
                    <th>Nom club</th>
                    <th>Moniteur</th>
                    <th></th>
                    <th></th>
                </tr>
            <?php
            foreach ($tab_ins as $inscription) {
                $ins = new Inscription($inscription['id_ins']);
                ?>
                <tr>
                    <td><?= $ins->get_prenom()." ".$ins->get_nom()?></td>
                    <td><?= $ins->get_tel()?></td>
                    <td><?= $ins->get_email()?></td>
                    <td><?= $ins->get_categorie_txt()?></td>
                    <td><?= $ins->get_club_txt()?></td>
                    <td><?= $ins->get_nom_club()?></td>
                    <td>
                        <?php
                        if($ins->get_nb_elv()){
                            echo "Moniteur avec ".$ins->get_nb_elv()." élève(s)";
                        }else{
                            echo"-";
                        }
                        ?>
                    </td>
                    <td>
                        <a href="../modification_ins.php?id_res=<?=$_GET['id_res']?>&id_ins=<?= $ins->get_id()?>"><button class="btn btn-warning" id_ins="<?= $ins->get_id()?>">Modifier</button></a>
                    </td>
                    <td>
                        <button class="btn btn-danger suppression" id_ins="<?= $ins->get_id()?>">Supprimer</button>
                    </td>
                </tr>

            <?php
            }
            ?>
            </table>
            <a href="index.php"><button class="btn btn-primary">Retour</button></a>
            <a href="generationExcel.php?id_res=<?= $res->get_id()?>"><button class="btn btn-primary">Générer un fichier excel</button></a>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>
<script src="../js/detail.js"></script>

