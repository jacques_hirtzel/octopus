$(function(){
    $('#date_res').on('change',function(){
        var d = new Date($(this).val());
        var day_tab = [
            'Dimanche',
            'Lundi',
            'Mardi',
            'Mercredi',
            'Jeudi',
            'Vendredi',
            'Samedi'
        ];
        $('#jour_res').html(day_tab[d.getDay()]);
    });

    // ANNULE LE formulaire d'ajout de réservation
    $(".close_mod_res").on("click",function(){
       $("#add_res_mod").modal("hide");
       $("#form_add_res")[0].reset();
    });
    // ajoute une réservation
    $("#btn_add_res").on("click",function(){
        if($('#close_res').is(':checked')) {
            close_res = 0;
        }else{
            close_res = 1;
        }
        $.post(
            "./json/add_res.json.php",
            {
                close_res: close_res,
                id_per: $("#id_per").val(),
                texte_res: $("#texte_res").val(),
                date_res: $("#date_res").val()
            },
            function(data){
                $("#add_res_mod").modal("hide");
                $("#form_add_res")[0].reset();
                location.reload();
            }
        )
    });

    $(".del_reservation").on("click",function () {
        id_res = $(this).attr("id_res");
        bootbox.confirm({
            title: "Supprimer une réservation",
            message: "Êtes-vous sûr de vouloir <b>supprimer la réservation du "+$(this).attr("date_res")+"</b> toutes les inscriptions liées seront supprimées également. <br>Merci d'avertir les participants avant de supprimer",
            buttons: {
                cancel: {
                    label: 'Annuler'
                },
                confirm: {
                    label:  'Supprimer'
                }
            },
            callback: function (result) {
                if(result == true){
                    del_res(id_res);
                }
            }
        });
    });

    function del_res(id_res){
        $.post(
            "./json/del_res.json.php",
            {
                id_res: id_res
            },
            function(data){
                location.reload();
            }
        )
    }

    function add_color_res(close_res){

    }
});