<?php
require_once substr(__dir__, 0, strpos(__dir__, "octopus")+strlen("octopus")) . "/config/config.inc.php";
$aut = "ADM_RES";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h3>Planning</h3>
        </div>

        <div class="panel-body">
        <button class="btn btn-primary add_res" data-toggle="modal" data-target="#add_res_mod">Ajouter une réservation</button>
            <br><br>
            <table class="table table-bordered table-striped table-condensed">
                <tr>
                    <th>Date</th>
                    <th>Nom Réservation</th>
                    <th>Responsable</th>
                    <th></th>

                </tr>
            <?php
            $res = new Reservation();
            $tab_res = $res->get_res_a_venir();
            foreach ($tab_res as $key => $reservation) {
               // $res = new Reservation($reservation['id_res']);
                $per_res= new Personne($reservation['id_per']);
                $date_res = date("d.m.Y", strtotime($reservation['date_res']));
                ?>
                <tr>
                    <td class="res_color" <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?= $date_res?></td>
                    <td class="res_color" <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?= $reservation['texte_res']?></td>
                    <td class="res_color" <?= (($reservation['close_res'] == 0) ? "style=\"opacity: 0.4\"" : "") ?>><?= $per_res->get_nom()." ".$per_res->get_prenom()?></td>
                    <td>
                        <a href="detailsAdmin.php?id_res=<?=$reservation['id_res']?>"><button class="btn btn-primary">Détails</button></a>
                        <a href="generationExcel.php?id_res=<?=$reservation['id_res']?> id_ins=<?=$ins['id_ins']?>"><button class="btn btn-primary">Générer un fichier excel</button></a>
                        <button class="btn btn-danger del_reservation" id_res="<?=$reservation['id_res']?>" date_res="<?= $date_res?>">Supprimer</button>
                    </td>
                </tr>
            <?php
            }
            ?>
            </table>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>
<?php
include (WAY."reservations/mod/add_reservation.mod.php");
?>
<script src="./js/reservations.js"></script>
