<?php


require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once substr(__dir__, 0, strpos(__dir__, "octopus")+strlen("octopus")) . "/config/config.inc.php";
/*$aut = "USR_INT";
require(WAY . "/includes/secure.inc.php");
header("Location: ". URL . "/reservations/index.php");*/
require_once(WAY . "/includes/autoload.inc.php");

$res = new Reservation($_GET['id_res']);
$per = new Personne($res->get_id_per());
$tab_ins = $res->get_ins();

$date_res = date("d.m.Y", strtotime($res->get_date()));

$colHeader = array("Nom, Prénom","n° Téléphone", "Email", "Catégorie", "Club/Privé" , "Nom club", "Nb Élève");
$spreadsheet = new Spreadsheet();
$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $date_res);
$spreadsheet ->addSheet($myWorkSheet,0);
$sheet = $spreadsheet->setActiveSheetIndex(0);

$sheet->mergeCells('A1:G1');
$sheet->mergeCells('A2:G2');
$sheet->mergeCells('A3:G3');

$sheet->setCellValue('A1',"Détails de la réservation du ".$date_res);
$sheet->setCellValue('A2',"Responsable : ".$per->get_nom()." ".$per->get_prenom());
$sheet->setCellValue('A3', $res->get_texte());


$sheet->fromArray([$colHeader], NULL, 'A4');

for($lettre = "A" ; $lettre <= "F"; $lettre++) {
    $sheet->getColumnDimension($lettre)->setAutoSize(true);
}

foreach ($tab_ins as $key => $inscription) {
    $index = $key+5;
    $ins = new Inscription($inscription['id_ins']);
    $sheet->fromArray([$ins->get_all()], NULL, 'A'.$index);
}



$styleDetailDate = ['font' => ['bold' => true, 'size' => 26],];
$sheet->getStyle('A1:G1')->applyFromArray($styleDetailDate);

$styleResponsable = ['font' => ['bold' => true, 'size' => 20],];
$sheet->getStyle('A2:G2')->applyFromArray($styleResponsable);

$styleActivity = ['font' => ['bold' => true, 'size' => 16],];
$sheet->getStyle('A3:G3')->applyFromArray($styleActivity);

$styleHeader = ['font' => ['bold' => true,],];
$sheet->getStyle('A4:G4')->applyFromArray($styleHeader);

$spreadsheet->removeSheetByIndex(1);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Réservation '.$date_res.'.xlsx"');
header('Cache-Control: max-age=0');

$writer = new Xlsx($spreadsheet);
$writer->save('php://output');

