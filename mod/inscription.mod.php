<!-- Modal -->
<div class="modal fade" id="add_ins_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <input type="hidden" name="id_res" id="id_res" value="">
    <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Formulaire d'inscription</h4>
      </div>

      <div class="modal-body">


          <form class="form-horizontal" id="inscription_form" method="post">
                      <div class="form-group row">
                          <label for="nom_ins" class="col-sm-2 col-form-label">Nom</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control" id="nom_ins" name="nom_ins" placeholder="votre nom">
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="prenom_ins" class="col-sm-2 col-form-label">Prénom</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control" id="prenom_ins" name="prenom_ins" placeholder="votre prénom">
                          </div>
                      </div>


                      <div class="form-group row">
                          <label for="tel_ins" class="col-sm-2 col-form-label">Téléphone</label>
                          <div class="col-sm-10">
                              <input type="text" class="form-control" id="tel_ins" name="tel_ins" placeholder="votre téléphone">
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="email_ins" class="col-sm-2 col-form-label">E-mail</label>
                          <div class="col-sm-10">
                              <input type="email" class="form-control" id="email_ins" name="email_ins" placeholder="votre mail">
                          </div>
                      </div>


                      <div class="form-group row">

                          <label for="type_ins" class="col-sm-2 col-form-label">Moniteur</label>
                          <div class="col-sm-2">
                                  <input type="checkbox" id="type_ins" name="type_ins" value= "0">
                          </div>

                          <div id="eleve">
                              <label for="nb_elv_ins" class="col-sm-2 col-form-label">No. d'élèves</label>
                              <div class="col-sm-2">
                                  <input type="number" class="form-control" id="nb_elv_ins" name="nb_elv_ins">
                              </div>
                          </div>


                      </div>

                      <div class="form-group row">
                          <label for="categorie_ins" class="col-sm-2 col-form-label" id="categorie_ins">Catégorie</label>
                          <div class="col-sm-10">
                              <input type="radio" value="1" name="categorie_ins">
                              <label for="1" class="type">Plongeur</label>
                              <br>
                              <input type="radio" value="2" name="categorie_ins">
                              <label for="2" class="type">Nageur</label>
                              <br>
                              <input type="radio" value="3" name="categorie_ins">
                              <label for="3" class="type">Apnéiste</label>
                              <br>
                          </div>
                      </div>



                      <div class="form-group row">
                          <label for="club_ins" class="col-sm-2 col-form-label">Membre d'un club/école</label>
                          <div class="col-sm-2" id="nom-club">
                              <input type="checkbox" value= "0" id="club_ins" name="club_ins">
                          </div>

                          <div id="club">
                              <label for="nom_club_ins" class="col-sm-2 col-form-label">Nom du club</label>
                              <div class="col-sm-2">
                                  <input type="text" class="form-control" id="nom_club_ins" name="nom_club_ins">
                              </div>
                          </div>
                      </div>
              </div>
      <div class="modal-footer">
          <div class="col-md-offset-8 col-md-2">
              <input type="submit" class="btn btn-primary" id="btn_add_ins" value="S'inscrire">
          </div>

          <div class="col-md-2">
              <input type="reset" class="form-control btn btn-warning" id="reset_conf" value="Annuler">
          </div>
          </form>
      </div>
    </div>

  </div>
</div>

