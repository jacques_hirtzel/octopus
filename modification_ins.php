<?php
require_once substr(__dir__, 0, strpos(__dir__, "octopus")+strlen("octopus")) . "/config/config.inc.php";
/*$aut = "USR_INT";
require(WAY . "/includes/secure.inc.php");
header("Location: ". URL . "/reservations/index.php");*/
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
$res = new Reservation($_GET['id_res']);
$per = new Personne($res->get_id_per());
$tab_ins = $res->get_ins();
$date_res = date("d.m.Y", strtotime($res->get_date()));
?>
    <div class="col-md-12">
        <div class="panel panel-primary">

            <div class="panel-heading">
                <h1>Modification de l'inscription du <?= $date_res?></h1>
                <h2>Responsable : <?= $per->get_nom()." ".$per->get_prenom() ?></h2>
                <p><?= $res->get_texte()?></p>
            </div>

            <div class="panel-body">
                    <table class="table table-bordered table-striped table-condensed">
                        <tr>
                            <th>Prénom, Nom</th>
                            <th>Nr.Téléphone</th>
                            <th>Email</th>
                            <th>Categorie</th>
                            <th>Club/Privé</th>
                            <th>Nom club</th>
                            <th>Nb.élèves</th>
                        </tr>
                        <?php
                            $ins = new Inscription($_GET['id_ins']);
                            $get_id_ins = $_GET['id_ins'];
                            ?>
                            <tr>
                                <td><?= $ins->get_prenom()." ".$ins->get_nom()?></td>
                                <td><?= $ins->get_tel()?></td>
                                <td><?= $ins->get_email()?></td>
                                <td><?= $ins->get_categorie_txt()?></td>
                                <td><?= $ins->get_club_txt()?></td>
                                <td><?= $ins->get_nom_club()?></td>
                                <td><input id="nb_elv" type="text" value="<?= $ins->get_nb_elv()?>"/></td>
                            </tr>
                    </table>
                <a href="reservations/detailsAdmin.php?id_res=<?=$_GET['id_res']?>"><button class="btn btn-primary">Retour</button></a>
                <button class="btn btn-warning confirmation" id_res="<?=$res->get_id()?>" id_ins="<?=$ins->get_id()?>">confirmer</button>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
<script src="./js/detail.js"></script>
